#!/bin/bash
source "$(dirname "$0")"/../shared

parse_flags () {
	local -A r_flags_4
	local flags
	local flag
	local mask
	local out

	mask=$1
	r_flags_4[U]="route usable"
	r_flags_4[G]="destination is a gateway"
	r_flags_4[H]="host entry"
	r_flags_4[R]="reinstate route after timeout"
	r_flags_4[D]="route created dynamically"
	r_flags_4[M]="route modified dynamically"

	flags[0]=U
	flags[1]=G
	flags[2]=H
	flags[3]=R
	flags[4]=D
	flags[5]=M

	for flag in "${!flags[@]}"; do
		if (( mask & (1<<flag) )); then
			out+=("${flags[flag]}${v:+(${r_flags_4[${flags[flag]}]})}")
		fi
	done
	local IFS
	IFS=","; printf '%s' "${out[*]:-NONE}"
}

parse_ipv4 () {
	local ip
	local byte
	local out

	ip=$1

	for byte in $(extract_bytes_le "$ip"); do
		out+=($(( byte )))
	done
	local IFS
	IFS="."; printf '%s' "${out[*]:-ERROR}"
}

parse_netmask () {
	local ip ip_b
	local bit mask
	local max

	ip_b=$(parse_ipv4 "$1")
	ip=$(le_val "$1" 4)

	bit=0 max=31
	while (( ip & 1 << max-- )); do
		((bit++))
	done
	printf '%s' "$ip_b($bit)"
}

if [[ ! -f /proc/net/route ]]; then
	exit 100
fi

# overdoing things for fun and no profit
while getopts :v args; do
	case "$args" in
		v)
			v=1
			;;
	esac
done
shift $(( OPTIND - 1 ))
# map routes to not read procfs multiple times
mapfile -t route < /proc/net/route
ints=(/sys/class/net/!(lo|bond*))

printf '# Iface Destination/mask Gateway Flags RefCnt Use Metric MTU Window IRTT\n\n'
for interface in "${ints[@]##*/}"; do
	while read -r int dst gw flags r_count use metric mask mtu window rtt; do
		if [[ $interface == "$int" ]]; then
			# FIXME: yep, I am bad with handling std*s. get it? :D
			printf '%s %s/%s %s %u %u %u %s %u %u %u\n' \
			"$int" "$(parse_ipv4 "0x$dst")" "$(parse_netmask "0x$mask")" "$(parse_ipv4 "0x$gw")" \
			"0x$r_count" "$use" "$metric" "$(parse_flags "0x$flags")" "$mtu" "0x$window" \
			"0x$rtt"
		fi
	done < <(printf '%s\n' "${route[@]}")
done
