#!/bin/bash
source "$(dirname "$0")"/../shared

VERSION=0.1
ME=${0##*/}

_calc_printf () {
	# Since Bash works only with signed %ll we need to get some floating
	# precision from other place. Awk is one of the most popular tools
	# there are so let's use it. No sanity checks if it exists, we will
	# just return 0 in case it doesn't.

	awk "{printf(\"$1\", $2)}" <<<: 2>/dev/null || printf "0"
}

calc_timestamp () {
	local seconds
	local minutes
	local hours

	seconds=$1
	if (( seconds <= 0 )); then
		return 1
	fi

	hours=$(( seconds / 3600 ))
	minutes=$(( (seconds / 60) - hours * 60 ))
	seconds=$(( seconds - 3600 * hours - minutes * 60 ))

	printf '%02u:%02u:%02u\n' "$hours" "$minutes" "$seconds"
}

convert_temp () {
	local temp

	temp=$1
	if (( kelvin == 0 )) && (( fahrenheit == 0 )); then
		printf '%u degrees C' "$temp"
	elif (( kelvin == 1 )); then
		_calc_printf "%0.2f Kelvin" "$temp + 273.15"
	elif (( fahrenheit == 1 )); then
		_calc_printf "%0.2f degrees F" "($temp * 9 / 5) + 32"
	fi
}

find_suspend_resume_device () {
	local dev
	dev=$(readlink -f /sys/dev/block/"$(</sys/power/resume)")
	dev=${dev##*/}

	if [[ $(</proc/swaps) == *"/dev/$dev"* ]]; then
		printf '%s\n' "$dev"
		return 0
	fi
	return 1
}

get_device_size () {
	# sectors to bytes
	printf '%u' $(( 512 * $(</sys/class/block/"$1"/size) ))
}

preparesuspend () {
	local modes mode m fm
	local disk_modes disk_mode d fd cd
	local delay

	mode=${1:-mem}
	if [[ ! -f /sys/power/state ]]; then
		printf 'System suspend is not supported\n' >&2
		return 1
	fi

	if ! amIroot; then
		printf 'You must be root to suspend the system\n' >&2
		return "$YOU_ARE_NOT_ROOT"
	fi

	IFS=":" read -r mode disk_mode <<<"$mode"
	read -ra modes </sys/power/state
	if [[ -f /sys/power/disk ]]; then
		read -ra disk_modes </sys/power/disk
	fi

	for m in "${modes[@]}"; do
		if [[ $m == "$mode" ]]; then
			fm=1; break
		fi
	done
	if (( fm == 0 )); then
		printf 'Suspend mode "%s" is not supported\n' "$mode" >&2
		return 1
	fi
	suspendmode=$mode
	if [[ $mode == disk ]] && [[ $disk_mode ]]; then
		for d in "${disk_modes[@]}"; do
			if [[ $d == "["*"]" ]]; then
				d=${d//[\[\]]}
				cd=$d
			fi
			if [[ $disk_mode == "$d" ]]; then
				fd=1
				if [[ $d != "$cd" ]]; then
					suspenddiskmode=$disk_mode
				else
					printf 'Disk mode "%s" is already set\n' "$d" >&2
				fi
				break
			fi
		done
		if (( fd == 0 )); then
			printf 'Disk mode "%s" is not supported, using "%s"\n' "$disk_mode" "$cd" >&2
			suspenddiskmode=$cd
		fi
		if [[ $disk_mode == shutdown ]] || [[ $disk_mode == reboot ]]; then
			if ! dev_resume=$(find_suspend_resume_device); then
				printf 'Resume device not found, refusing to suspend system to disk\n' >&2
				return 1
			fi
			if (( $(get_device_size "$dev_resume") < $(</sys/power/image_size) )); then
				printf 'WARNING: /dev/%s is smaller than the image size kernel will try to compress the memory to\n' >&2 "$dev_resume"
			fi
		fi
	fi
}

suspend () {
	local delay=${DELAY:-5}

	trap 'exit "$delay"' INT HUP 0
	while (( delay )); do
		printf 'Suspending system ("%s"%s) in %u...(press ctrl+c to abort)\r' \
		    "$suspendmode" "${dev_resume:+, resume_device=/dev/$dev_resume}" $((delay--))
		sleep 1s
	done
	trap - INT HUP 0
	sync
	if [[ $suspenddiskmode ]]; then
		printf '%s' "$suspenddiskmode" >/sys/power/disk
	fi
	printf '%s' "$suspendmode" >/sys/power/state
	# reached when wake up succeeds
	return 0
}

# override the builtin
getopts () {
	local opts o
	local args a sargs s ss
	local -A processed_args

	args=("$@")
	opts=(
			-a --ac-adapter 0
			-b --battery    0
			-c --cooling    0
			-d --directory  1
			-f --fahrenheit 0
			-h --help       0
			-i --details    0
			-k --kelvin     0
			-p --proc       0
			-s --suspend    1
			-t --thermal    0
			-v --version    0
			-V --everything 0
			-D --debug      0
		 )

	if (( ${#args[@]} == 0 )); then
		help
		return 0
	fi

	# dumb way of handling short opts bundled together (e.g. -probit)
	sargs=("${args[@]}")
	for s in "${sargs[@]}"; do
		if [[ $s == -!(*-*) ]] \
		&& (( ${#s} > 2 )); then
			for ((ss=1; ss < ${#s}; ss++)); do
				args+=("-${s:ss:1}")
			done
		fi
	done

	for ((a=0; a < ${#args[@]}; a++)); do
		if (( processed_args["${args[a]}"] )); then
			continue
		fi
		for ((o=0; o < ${#opts[@]}; o += 3)); do
			if [[ ${args[a]} == "${opts[o]}" ]] \
			|| [[ ${args[a]} == "${opts[o+1]}" ]]; then
				if (( opts[o + 2] )); then
					setarg "${args[a]}" "${args[a+1]}"
					((++a))
				else
					setarg "${args[a]}"
				fi
				processed_args["${opts[o]}"]=1
				processed_args["${opts[o+1]}"]=1
				continue 2
			fi
		done
	done
	return 0
}

setarg () {
	local arg subarg

	arg=$1
	subarg=${2:-}

	# keep in sync with opts[] in getopts()
	case "${arg##-?(-)}" in
		a|ac-adapter)
			adapter=1 ;;
		b|battery)
			battery=1 ;;
		c|cooling)
			cooling=1 ;;
		d|directory)
			acpi_dir=$subarg; [[ -d $acpi_dir ]] ;;
		f|fahrenheit)
			fahrenheit=1 ;;
		h|help)
			help=1 ;;
		i|details)
			details=1 ;;
		k|kelvin)
			kelvin=1 ;;
		p|proc)
			: ;; # acpi_dir=$proc_dir ;;
		t|thermal)
			thermal=1 ;;
		s|suspend)
			if preparesuspend "$subarg"; then
				suspend=1
			fi
			 ;;
		v|version)
			version=1 ;;
		V|everything)
			all=1 details=1 ;;
		D|debug)
			debug=1 ;;
	esac || return 1
}

processmisc () {
	if (( help == 0 )) \
	&& (( version == 0 )); then
		return 0
	fi

	if (( help )); then
		help
	fi
	if (( version )); then
		printversion
	fi
	return 1
}

help () {
	cat <<-HELP
		Usage: $ME [OPTION] ...

		Shows information from sysfs, such as battery status or thermal information.

		-b, --battery                         Battery information,
		-i, --details                         Show additional details if available,
		-a, --ac-adapter                      AC adapter information,
		-t, --thermal                         Thermal information,
		-c, --cooling                         Cooling information,
		-V, --everything                      Show all available info,
		-f, --fahrenheit                      Use fahrenheit as the temperature unit,
		-k, --kelvin                          Use kelvin as the temperature unit,
		-d, --directory <dir>                 Path to ACPI info (<dir>/{thermal,power_supply}),
		-p, --proc                            Use old proc interface instead of new sys interface (not supported),
		-s, --suspend [state[:disk_state]]    Suspend system to given state ("mem" is default),
		-h, --help                            Display this help and exit,
		-v, --version                         Output version information and exit.

		By default, $ME displays this text.

		$ME is meant to resemble acpi (Paul Telford <pxt@debian.org> Michael Meskes <meskes@debian.org>) and
		acpitool (David Leemans) as much as possible. The point in writing it was to easily adjust to changes
		in recent kernels, without modifying their source (Yes, maintaining userspace tools as such is easier
		when they are kept in simple form, e.g. Bash).

	HELP
}

printversion () { printf '%s %s\n' "$ME" "$VERSION" ; }

main () {
	processmisc || return 0

	if (( all == 1 )); then
		processbattery
		processadapter
		processcooling
		processthermal
		exit 0
	fi

	if (( battery == 1 )); then
		processbattery
	fi
	if (( adapter == 1 )); then
		processadapter
	fi
	if (( cooling == 1 )); then
		processcooling
	fi
	if (( thermal == 1 )); then
		processthermal
	fi
	if (( suspend == 1 )); then
		suspend
	fi
}

processbattery () {
	local battery

	for battery in "$acpi_dir"/power_supply/BAT+([0-9]); do
		getbattery "$battery"
	done
}

processadapter () {
	local adapter
	# names can be a bit generic
	local count
	for adapter in "$acpi_dir"/power_supply/AC*; do
		count=$((count++)) getadapter "$adapter"
	done
}

processcooling () {
	local cool_device

	for cool_device in "$acpi_dir"/thermal/cooling_device+([0-9]); do
		getcooling "$cool_device"
	done
}

processthermal () {
	local zone

	for zone in "$acpi_dir"/thermal/thermal_zone+([0-9]); do
		getthermalzone "$zone"
	done
}

# thermal class doesn't register any uevents
getthermalzone () { # <- note the difference
	local zone
	local type
	local temp
	local mode
	local policy
	local device path
	local trip_point

	zone=$1
	if [[ -f $zone/type ]]; then
		type=$(<"$zone/type")
	fi
	if [[ -f $zone/temp ]]; then
		temp=$(<"$zone/temp")
	fi
	if [[ -f $zone/mode ]]; then
		mode=$(<"$zone/mode")
	fi
	if [[ -f $zone/policy ]]; then
		policy=$(<"$zone/policy")
	fi
	if [[ -f $zone/device/path ]]; then
		device=$(readlink -f "$zone/device")
		device=${device##*/}
		path=$(<"$zone/device/path")
	fi
	temp=$(convert_temp $(( temp / 1000 )))
	printf 'Thermal Zone %u: %s' "${zone#*_zone}" "$temp"
	if (( details )); then
		printf ' (%s, %s, %s)' "${type:-UNKNOWN}" "${mode:-UNKNOWN}" "${policy:-UNKNOWN}"
		if [[ $device ]] && [[ $path ]]; then
			printf ' - %s:%s' "$device" "$path"
		fi
		printf '\n'
		for trip_point in "$zone"/trip_point*type; do
			id=${trip_point#*point_} id=${id%_*}
			type=$(<"$trip_point")
			temp=$(<"$zone/trip_point_${id}_temp")
			temp=$(convert_temp $(( temp / 1000 )))
			printf '    Trip Point %u: %s, %s\n' "$id" "$type" "$temp"
		done
	else
		printf '\n'
	fi
}

# thermal class doesn't register any uevents
getcooling () { # <- note the difference
	local dev
	local type
	local state
	local max_state
	local desc

	dev=$1
	desc[0]="cooling off"
	desc[1]="cooling on"

	if [[ -f $dev/type ]]; then
		type=$(<"$dev/type")
	fi
	if [[ -f $dev/cur_state ]]; then
		state=$(<"$dev/cur_state")
	fi
	if [[ -f $dev/max_state ]]; then
		max_state=$(<"$dev/max_state")
	fi
	printf 'Cooling %u: %s:' "${dev#*_device}" "${type:-UNKNOWN}"
	# intel's powerclamp driver handles states a bit differently.
	if [[ $type == intel_powerclamp ]]; then
		if (( state == -1 )); then
			printf ' Disabled\n'
		else
			printf ' %u%% of %u%%\n' "$state" "$max_state"
		fi
	else
		printf ' %u (%s) of %u\n' "$state" "${desc[state ? 1 : 0]}" "$max_state"
	fi
}

getadapter () (
	local states

	states[0]=offline
	states[1]=online

	source "$1/uevent"
	printf '%s %u: %s' "$POWER_SUPPLY_NAME" "$count" "${states[POWER_SUPPLY_ONLINE]}"
	if (( details )); then
		if [[ -f $1/type ]]; then
			printf ', %s' "$(<"$1/type")"
		fi
		if [[ -f $1/device/path ]]; then
			printf ', %s:%s' \
			    "$(basename "$(readlink -f "$1/device")")" \
			    "$(<"$1/device/path")"
		fi
	fi
	printf '\n'
)

getbattery () (
	# There may be whitespaces in the values, e.g. manufacturer name, escape them.
	escapedsourcevars "$1/uevent"

	if (( POWER_SUPPLY_PRESENT == 0 )); then
		printf '%s: slot is empty\n' "$POWER_SUPPLY_NAME"
	else
		local unit=mWh

		if [[ ! ${!POWER_SUPPLY_ENERGY*} ]]; then
			POWER_SUPPLY_ENERGY_NOW=$POWER_SUPPLY_CHARGE_NOW
			POWER_SUPPLY_ENERGY_FULL=$POWER_SUPPLY_CHARGE_FULL
			POWER_SUPPLY_ENERGY_FULL_DESIGN=$POWER_SUPPLY_CHARGE_FULL_DESIGN
			if [[ -v POWER_SUPPLY_CURRENT_NOW ]]; then
				POWER_SUPPLY_POWER_NOW=$POWER_SUPPLY_CURRENT_NOW
			fi
			unit=mAh
		fi
		# normalize but hide es of (( )) evaluation from the debugger
		: $(( POWER_SUPPLY_ENERGY_NOW /= 1000 ))
		: $(( POWER_SUPPLY_ENERGY_FULL /= 1000 ))
		: $(( POWER_SUPPLY_ENERGY_FULL_DESIGN /= 1000 ))
		: $(( POWER_SUPPLY_POWER_NOW /= 1000 ))
		POWER_SUPPLY_STATUS=${POWER_SUPPLY_STATUS,,}
		# calc extra stuff
		if (( POWER_SUPPLY_POWER_NOW )); then
			if [[ $POWER_SUPPLY_STATUS == discharging ]]; then
				time_left=$(calc_timestamp "$(_calc_printf "%u" "3600 * ($POWER_SUPPLY_ENERGY_NOW / $POWER_SUPPLY_POWER_NOW)")")
			elif [[ $POWER_SUPPLY_STATUS == charging ]]; then
				time_left=$(calc_timestamp "$(_calc_printf "%u" "3600 * (($POWER_SUPPLY_ENERGY_FULL - $POWER_SUPPLY_ENERGY_NOW) / $POWER_SUPPLY_POWER_NOW)")")
			fi
		fi
		energy_percentage=$(_calc_printf "%0.2f" "$POWER_SUPPLY_ENERGY_NOW * 100 / $POWER_SUPPLY_ENERGY_FULL")
		capacity_percentage=$(_calc_printf "%0.2f" "$POWER_SUPPLY_ENERGY_FULL * 100 / $POWER_SUPPLY_ENERGY_FULL_DESIGN")
		cat <<-BATTERY
			Battery $POWER_SUPPLY_NAME     : present
			   Remaining capacity : $POWER_SUPPLY_ENERGY_NOW $unit, ${energy_percentage}%, ${time_left:-N/A}
			   Design capacity    : $POWER_SUPPLY_ENERGY_FULL_DESIGN $unit
		BATTERY
		if (( $(_calc_printf "%u" "$capacity_percentage > 100 ? 0 : 1") )); then
			printf '   Last full capacity : %s %s (%s%% of Design capacity)\n' \
			  "$POWER_SUPPLY_ENERGY_FULL" "$unit" "$capacity_percentage"
		else
			printf '   Last full capacity : %s %s\n' \
			  "$POWER_SUPPLY_ENERGY_FULL" "$unit"
		fi
		cat <<-BATTERY
			   Charging state     : $POWER_SUPPLY_STATUS
		BATTERY
		if (( details )); then
			if (( $(_calc_printf "%u" "$capacity_percentage > 100 ? 0 : 1") )); then
				cat <<-BATTERY
					    Capacity loss     : $(_calc_printf "%0.2f%%" "100 - $capacity_percentage")
				BATTERY
			fi
			cat <<-BATTERY
				    Battery type      : ${POWER_SUPPLY_TECHNOLOGY:-N/A}
				    Model number      : ${POWER_SUPPLY_MODEL_NAME:-N/A}
				    Serial number     : ${POWER_SUPPLY_SERIAL_NUMBER:-N/A}
				    Manufacturer      : ${POWER_SUPPLY_MANUFACTURER:-N/A}
			BATTERY
			if [[ -f $1/device/path ]]; then
				cat <<-BATTERY
					    Device Path       : $(basename "$(readlink -f "$1/device")"):$(<"$1/device/path")
				BATTERY
			fi
		fi
	fi
)

# classes
battery=0 adapter=0
thermal=0 cooling=0

# extra info
details=0 all=0

# interfaces - default to sysfs
sysfs_dir=/sys/class # proc_dir=/proc/acpi
acpi_dir=$sysfs_dir

# misc
help=0 version=0 debug=0

# suspend
suspend=0
suspendmode="" suspenddiskmode="" dev_resume=""

getopts "$@"
if (( debug == 1 )); then
	enable_debug
fi
main
