#!/bin/bash
source "$(dirname "$0")"/../shared

errors[0x1]="Not a regular file"
errors[0x2]="Failed to verify magic"
errors[0x3]="No x86 boot sector magic found"
errors[0x4]="Old protocol kernel"
errors[0x5]="zImage found, not bzImage"
errors[0x6]="Not a relocatable bzImage"

trap 'e=$?; (( e )) && echo "${errors[e]:-ERROR}" >&2' EXIT

check_magic () {
	local magic m
	local string

	m=("$@")
	magic=HdrS

	string=$(hex_to_string "${m[@]}")
	if [[ $string == "$magic" ]]; then
		return 0
	fi
	return 1
}

check_boot_sector () {
	(( $(le "$@") == 0xaa55 ))
}

check_protocol_version () {
	(( $(le "$@") >= 0x020c ))
}

check_if_not_zImage () {
	local b
	b=$1; (( b & 1 ))
}

check_if_reloc () {
	(( $(le "$@") & 3 == 3 ))
}

kernel=$1
if [[ ! -f $kernel ]]; then
	exit 1
fi

check_magic $(hex -s0x202 -n4 "$kernel") || exit 2
check_boot_sector $(hex -s0x1fe -n2 "$kernel") || exit 3
check_protocol_version $(hex -s0x206 -n2 "$kernel") || exit 4
check_if_not_zImage $(hex -s0x211 -n1 "$kernel") || exit 5
check_if_reloc $(hex -s0x236 -n2 "$kernel") || exit 6

printf '%s is a bzImage64 kernel\n' "$kernel"
