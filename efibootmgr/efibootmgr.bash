#!/usr/bin/env bash
shopt -s {ext,null}glob

EFI_GLOBAL_VARIABLE_GUID=8be4df61-93ca-11d2-aa0d-00e098032b8c
ADDRESS_RANGE_MIRROR_VARIABLE_GUID=7b9be2e0-e28a-4197-ad3e-32f062f9462c
EFI_LOADER_ENTRY_GUID=4a67b082-0a4c-41cf-b6c7-440b29bb8c4f
EFI_SHIM_LOCK_GUID=605dab50-e046-4300-abb6-3dd810dd8b23
EFI_AUTO_CREATED_GUID=8108ac4e-9f11-4d59-850e-e21a522c59b2
NULL_GUID=00000000-0000-0000-0000-000000000000
EFIVARS=/sys/firmware/efi/efivars

is_efivars() { [[ -e $EFIVARS ]]; }

genguid() {
	local guid=$NULL_GUID
	if [[ -e /proc/sys/kernel/random/uuid ]]; then
		guid=$(< /proc/sys/kernel/random/uuid)
	else
		printf -v guid '%08x-%04x-%04x-%04x-%12x' \
			"$SRANDOM" \
			$((SRANDOM & 0xffff)) \
			$((SRANDOM & 0xffff)) \
			$((SRANDOM & 0xffff)) \
			$((SRANDOM | (SRANDOM & 0xffff) << 32))
	fi
	echo "$guid"
}

platform() {
	local platform=()

	if [[ -e /sys/class/dmi/id/board_vendor ]]; then
		platform+=("$(< /sys/class/dmi/id/board_vendor)")
	fi

	if [[ -e /sys/class/dmi/id/board_name ]]; then
		platform+=("$(< /sys/class/dmi/id/board_name)")
	fi

	if [[ -e /sys/class/dmi/id/product_family ]]; then
		platform+=("$(< /sys/class/dmi/id/product_family)")
	fi

	local IFS="-"
	echo "${platform[*]:-N/A}"
}

bytes_to_x509() {
	local bytes=("$@")

	# We skip the owner guid. The base here can then be used with PEM format.
	# Note that though we keep the encoding on a single line openssl may
	# complain if you don't take that into an account while creating the PEM
	# file. So it's easier to just decode it back to bin:
	# $ efibootmgr.bash -f MokListRT#
	# MokListRT (bootservice-access,runtime-access): cert_x509{BASE64[,BASE64...]}
	# $ echo -n "$BASE64" | base64 -d | openssl x509 -inform DER -text -noout
	to_bin "${bytes[@]:16}" | base64 -w0 || flat_bytes "${bytes[@]}"
}

bytes_to_uuid() {
	local bytes=("$@") uuid

	((${#bytes[@]} == 16)) || return 1

	uuid+=("$(dword "${bytes[@]::4}")")
	uuid+=("$(word "${bytes[@]:4:2}")")
	uuid+=("$(word "${bytes[@]:6:2}")")
	uuid+=("$(word_l "${bytes[@]:8:2}")")
	uuid+=("$(abytes_l 6 "${bytes[@]:10:6}")")

	local IFS="-"
	echo "${uuid[*]//0x/}"
}

uuid_to_bytes() {
	local uuidv=$1 bytes=()
	local hex="[a-fA-F0-9]" uuidm

	[[ $uuidv =~ ($hex{8})-($hex{4})-($hex{4})-($hex{4})-($hex{12}) ]] || return 1
	uuidm=("${BASH_REMATCH[@]/#/0x}")

	# shellcheck disable=SC2207
	bytes+=($(val_to_bytes "${uuidm[1]}" 4))
	# shellcheck disable=SC2207
	bytes+=($(val_to_bytes "${uuidm[2]}" 2))
	# shellcheck disable=SC2207
	bytes+=($(val_to_bytes "${uuidm[3]}" 2))
	# shellcheck disable=SC2207
	bytes+=($(val_to_bytes_l "${uuidm[4]}" 2))
	# shellcheck disable=SC2207
	bytes+=($(val_to_bytes_l "${uuidm[5]}" 6))

	printf '%s\n' "${bytes[@]}"
}

hex() { hexdump -ve '1/1 "0x%02x\n"' "$@"; }
printable() { hexdump -ve '1/1 "%s"'; }
nprintable() { hexdump -ve '1/1 "%_p"'; }
plain_hex() { printf '0x%x' "$1"; }
hex_efivar() { printf '%04X' "$1"; }
dec_efivar() { printf '%u' "$1"; }

parse_efivars() {
	local efivars=("$@") efivar
	local name guid data

	for efivar in "${efivars[@]}"; do
		# Silently skip efivars which don't have r bit set
		[[ -r $efivar ]] || continue
		# shellcheck disable=SC2207
		data=($(hex "$efivar"))
		((${#data[@]} > 0)) || continue
		efivar=${efivar##*/}
		name=${efivar%-*-*-*-*-*}
		guid=${efivar#"$name-"}
		parse_efivar "$name" "$guid" "${data[@]}"
	done
}

parse_efivar() {
	local name=$1 guid=$2 data=("${@:3}")
	local generic_desc main_desc debug_desc

	generic_desc=$(parse_generic_efivar "$name" "${data[@]::4}")
	debug_desc=$(debug_efivar "${data[@]}")

	data=("${data[@]:4}") guid=$(detect_guids "$guid")
	case "$guid" in
		global_efivars_guid) main_desc=$(parse_global_efivar "$name" "${data[@]}") ;;
		address_range_mirror) main_desc=$(parse_range_mirror_efivar "$name" "${data[@]}") ;;
		loader_efivars_guid) main_desc=$(parse_loader_efivar "$name" "${data[@]}") ;;
		shim_efivars_guid) main_desc=$(parse_shim_efivar "$name" "${data[@]}") ;;
		*)
			# Everything that's not defined by the spec goes here. Map it based on the
			# name and vendor guid to make sure we have exact match with what we actually
			# support.
			local desc
			case "$name:$guid" in
				BootOrderDefault:*)
					# Spotted under LENOVO T480 (20L6SBYM0W, SDK0J40697 WIN)
					;&
				LastBootOrder:*)
					# Spotted under LENOVO T480 (20L6SBYM0W, SDK0J40697 WIN)
					;&
				ProtectedBootOptions:*)
					# Spotted under LENOVO T480 (20L6SBYM0W, SDK0J40697 WIN)
					main_desc=$(parse_global_efivar BootOrder "${data[@]}")
					;;&
				LastBootCurrent:*)
					# Spotted under LENOVO T480 (20L6SBYM0W, SDK0J40697 WIN)
					main_desc=$(parse_global_efivar BootCurrent "${data[@]}")
					;;&
				lBoot*)
					# Spotted under LENOVO T480 (20L6SBYM0W, SDK0J40697 WIN)
					main_desc=$(parse_global_efivar "${name#l}" "${data[@]}")
					;;&
				VMMBootOrder*:qemu_bootorder)
					# OVMF's efivars which store QEMU's bootorder paths
					main_desc=$(parse_efivar_boot_entry_filepaths "${data[@]}")
					;;&
				VarErrorFlag:edk_var_error_flag)
					# Describes DXE env's state
					main_desc=$(parse_efivar_var_error_flag "${data[@]}")
					;;&
				MTC:mtc_vendor)
					# high dword of the platform's monotonic counter
					main_desc=$(dword "${data[@]}")
					;;&
				*)
					if [[ -z $main_desc ]]; then
						main_desc=UNKNOWN-$guid
					fi
				;;
			esac
	esac

	printf '%s: %s\n' "$generic_desc" "$main_desc"
	if [[ -n $debug_desc ]]; then
		printf '%s\n' "$debug_desc"
	fi
}

debug_efivar() {
	local data=("$@") efivar_raw

	((debug == 1)) || return 0

	mapfile -t efivar_raw < <(raw_data "${data[@]}")
	printf ' %s\n' "${efivar_raw[@]}"
}

parse_global_efivar() {
	local name=$1 data=("${@:2}")

	case "$name" in
		AuditMode)
			local audit_mode=([0]=disabled [1]=enabled)
			efivar_desc=${audit_mode[data[0]}
			;;
		BootCurrent | BootNext)
			efivar_desc=$(hex_efivar "$(word "${data[@]}")")
			;;
		BootOrder)
			efivar_desc=$(parse_efivar_boot_order "${data[@]}")
			;;
		BootOptionSupport)
			efivar_desc=$(parse_efivar_boot_opts "${data[@]}")
			;;
		Boot* | PlatformRecovery*)
			efivar_desc=$(parse_efivar_boot_entry "${data[@]}")
			;;
		Con* | Err*)
			efivar_desc=$(parse_efivar_boot_entry_filepaths "${data[@]}")
			;;
		HwErrRecSupport)
			local hwerrrec_mode=([0]=no-support [1]=supported)
			efivar_desc=${hwerrrec_mode[$(word "${data[@]::2}")]}
			;;
		Key*)
			efivar_desc=$(parse_efivar_key_entry "${data[@]}")
			;;
		*Lang*)
			efivar_desc=$(bytes_to_string "${data[@]}")
			;;
		OsIndications*)
			efivar_desc=$(parse_efivar_osindications_entry "${data[@]}")
			;;
		SecureBoot | SetupMode)
			local sec_mode=([0]=disabled [1]=enabled)
			efivar_desc=${sec_mode[data[0]]}
			;;
		SignatureSupport)
			efivar_desc=$(parse_efivar_signature_entry "${data[@]}")
			;;
		Timeout)
			efivar_desc=$(printf '%u seconds' "$(word "${data[@]}")")
			;;
		VendorKeys)
			efivar_desc=$(parse_efivar_vendorkeys_entry "${data[@]}")
			;;
		*) return 0 ;;
	esac

	printf '%s\n' "${efivar_desc:-UNKNOWN}"
}

parse_generic_efivar() {
	local name=$1 data=("${@:2}")
	local attributes parsed_attributes efivar_desc

	attributes=("${data[@]::4}")
	parsed_attributes=$(parse_efivar_attributes "${attributes[@]}")

	printf '%s (%s)\n' "$name" "$parsed_attributes"
}

parse_range_mirror_efivar() {
	local name=$1 data=("${@:2}")

	local version
	local below4g
	local above4g
	local status
	local bool=([0]=false [1]=true)

	version=${data[0]}
	below4g=${data[1]}
	above4g=$(word "${data[@]:2:2}")
	status=${data[4]}

	printf -v above4g '%d.%.2d' $((above4g / 100)) $((above4g % 100))

	case "$name" in
		MirrorCurrent | MirrorRequest)
			case $((status)) in
				1) efivar_desc="address range mirror not supported" ;;
				2) efivar_desc="Invalid version number" ;;
				3) efivar_desc="above4g > 50%" ;;
				4) efivar_desc="DIMM configuration does not allow mirror" ;;
				5) efivar_desc="OEM" ;;
				*) efivar_desc=$status ;;
			esac
			;;
	esac

	efivar_desc+=",$above4g,${bool[below4g]},$version"

	printf '%s\n' "${efivar_desc:-UNKNOWN}"
}

parse_loader_efivar() {
	local name=$1 data=("${@:2}")

	case "$name" in
		LoaderInfo) ;&
		LoaderEntryRebootReason) ;&
		LoaderEntryOneShot) ;&
		LoaderFirmwareInfo) ;&
		LoaderFirmwareType) ;&
		LoaderImageIdentifier) ;&
		LoaderConfigTimeout) ;&
		LoaderConfigTimeoutOneShot) ;&
		LoaderTimeInitUSec) ;&
		LoaderTimeExecUSec) ;&
		LoaderTimeMenuUSec) ;&
		LoaderDevicePartUUID) efivar_desc=$(ucs2_to_string "${data[@]}") ;;
		LoaderSystemToken) efivar_desc="Random Binary Data{$(flat_bytes "${data[@]}")}" ;;
		LoaderEntries) efivar_desc=$(ucs2_to_string_list "${data[@]}") ;;
		LoaderEntrySelected) efivar_desc=$(ucs2_to_string "${data[@]}") ;;
		LoaderFeatures) efivar_desc=$(parse_efivar_loader_features_entry "${data[@]}") ;;
	esac

	printf '%s\n' "${efivar_desc:-UNKNOWN}"
}

parse_shim_efivar() {
	local name=$1 data=("${@:2}")

	case "$name" in
		ShimRetainProtocol)
			local protocol=([0]=not-retained [1]=retained)
			efivar_desc=${protocol[data[0] > 0 ? 1 : 0]}
			;;
		MokListRT | MokListXRT) efivar_desc=$(parse_efivar_shim_mokrt_entry "${data[@]}") ;;
		MokListTrustedRT)
			local trust=([0]=no-trust [1]=trust)
			efivar_desc=${trust[data[0] > 0 ? 1 : 0]}
			;;
	esac

	printf '%s\n' "${efivar_desc:-UNKNOWN}"
}

parse_efivar_shim_mokrt_entry() {
	local data=("$@") copy_data=()

	local signature_type
	local signature_list_size
	local signature_header_size
	local signature_size
	local signature signatures

	copy_data=("${data[@]}")

	while ((${#copy_data[@]} > 0)); do
		signature_type=$(detect_guids "$(bytes_to_uuid "${copy_data[@]::16}")")
		signature_list_size=$(dword "${copy_data[@]:16:4}")
		# shellcheck disable=SC2034
		signature_header_size=$(dword "${copy_data[@]:20:4}")
		signature_size=$(dword "${copy_data[@]:24:4}")
		signature=("${copy_data[@]:28:signature_size}")
		copy_data=("${copy_data[@]:signature_list_size}")
		signatures=${signatures:+$signatures,}$signature_type
		case "$signature_type" in
			cert_x509) signatures+="{$(bytes_to_x509 "${signature[@]}")}" ;;
			*) signatures+="{$(flat_bytes "${signature[@]}")}" ;;
		esac
	done
	echo "$signatures"
}

parse_efivar_loader_features_entry() {
	local data=("$@") fmask
	local features_map=() features feature

	features_map[0x0001]="timeout"
	features_map[0x0002]="timeout-oneshot"
	features_map[0x0004]="entry-default"
	features_map[0x0008]="entry-oneshot"
	features_map[0x0010]="boot-counting"
	features_map[0x0020]="xesp"
	features_map[0x0040]="seed"
	features_map[0x0080]="load-driver"
	features_map[0x0100]="sort-key"
	features_map[0x0200]="saved-entry"
	features_map[0x0400]="device-tree"
	features_map[0x0800]="sec-boot-enroll"
	features_map[0x1000]="retain-shim"
	features_map[0x2000]="menu-disabled"

	fmask=$(qword "${data[@]::8}")

	for feature in "${!features_map[@]}"; do
		if ((fmask & feature)); then
			features+=("${features_map[feature]}")
		fi
	done

	local IFS=","
	echo "${features[*]}"
}

parse_efivar_boot_opts() {
	local data=("$@")
	local opts_map=() opts_mask=0x0 opt opts

	opts_map[0x01]=support-key
	opts_map[0x02]=support-app
	opts_map[0x10]=support-sysprep

	opts_mask=$(dword "${data[@]}")

	for opt in "${!opts_map[@]}"; do
		((opts_mask & opt)) || continue
		opts+=("${opts_map[opt]}")
	done

	if ((opts_mask & 0x1)); then
		opts+=("count=$(((opts_mask >> 8) & 0xff))")
	fi

	local IFS=","
	echo "${opts[*]}"

}

parse_efivar_boot_order() {
	local data=("$@")
	local entry offset no_entries boot_order=()

	no_entries=$((${#data[@]} / 2))

	for ((entry = 0, offset = 0; entry < no_entries; entry++, offset += 2)); do
		# shellcheck disable=SC2207
		boot_order+=($(hex_efivar "$(word "${data[@]:offset:2}")"))
	done

	local IFS=","
	echo "${boot_order[*]}"
}

parse_efivar_boot_entry() {
	local data=("$@") attributes_map=()
	local attribute attribute_mask=0x0 attributes_parsed
	local description filepathdesc

	attributes_map[0x01]="*" # This is how efibootmgr marks ACTIVE entry
	attributes_map[0x02]=force-reconnect
	attributes_map[0x08]=hidden

	attribute_mask=$(dword "${data[@]}")
	# shift the loadopt attributes and file_path_list_length
	description=$(parse_efivar_boot_entry_desc "${data[@]:6}")
	# 2 bytes per char + 0x0000 (NUL)
	filepathdesc=$(parse_efivar_boot_entry_filepaths "${data[@]:6 + ${#description} * 2 + 2}")

	for attribute in "${!attributes_map[@]}"; do
		((attribute_mask & attribute)) || continue
		attributes_parsed+=("${attributes_map[attribute]}")
	done

	if ((((attribute_mask >> 8) & 0x1f) == 0x1)); then
		attributes_parsed+=(APP)
	fi

	if ((debug == 1)); then
		attributes_parsed+=("$(crc32 "${data[@]}")")
	fi

	local IFS=","
	echo "(${attributes_parsed[*]:--}) $description $filepathdesc"
}

parse_efivar_boot_entry_desc() {
	local description=("$@") description_str=""

	description_str=$(ucs2_to_string "${description[@]}")
	echo "$description_str"
}

parse_efivar_boot_entry_filepaths() {
	local filepaths=("$@") filepath

	# efidp_header
	local type
	local subtype
	local length
	local efidp_header=4

	# Description of the given file type
	local filepath_desc=()
	local filepath_data=()
	local filepaths_list=()

	while ((${#filepaths[@]} > 0)); do
		type=${filepaths[0]}
		subtype=${filepaths[1]}
		length=$(word "${filepaths[@]:2:2}")
		filepath=""

		filepath_data=("${filepaths[@]:efidp_header:length - efidp_header}")
		filepaths=("${filepaths[@]:length}")
		case $((type)) in
			1) ;&
			2) filepath=$(format_gen_device "$type" "$subtype" "${filepath_data[@]}") ;;
			3) filepath=$(format_msg_device "$subtype" "${filepath_data[@]}") ;;
			4) filepath=$(format_media_type "$subtype" "${filepath_data[@]}") ;;
			5) filepath=$(format_bbs_type "$subtype" "${filepath_data[@]}") ;;
			127)
				filepaths_list+=("$(IFS="/"; echo "${filepath_desc[*]}")")
				if ((subtype == 255)); then
					break
				fi
				filepath_desc=()
				continue
				;;
		esac

		if [[ -n $filepath ]]; then
			filepath_desc+=("$filepath")
		fi
	done

	# Check for optional data
	if ((${#filepaths[@]} > 0)); then
		filepaths_list[-1]+="{$(detect_guids "$(bytes_to_uuid "${filepaths[@]::16}")")}"
	fi

	local IFS=";"
	if ((${#filepaths_list[@]} > 0)); then
		echo "${filepaths_list[*]}"
	fi
}

parse_efivar_key_entry() {
	local key_data=("$@")

	# struct abstract
	local key_data
	local key_data_map=() key_data_parsed=""
	local revision
	local key_count key_count_idx=0
	local crc32
	local boot_opt
	local _key_presses=() key_presses=()

	local key scan input_key

	key_data=$(dword "${key_data[@]::4}")
	crc32=$(dword "${key_data[@]:4:4}")
	boot_opt=$(hex_efivar "$(word "${key_data[@]:8:2}")")

	_key_presses=("${key_data[@]:10}")

	key_data_map[1 << 8]=SHIFT
	key_data_map[1 << 9]=CTRL
	key_data_map[1 << 10]=ALT
	key_data_map[1 << 11]=LOGO
	key_data_map[1 << 12]=MENU
	key_data_map[1 << 13]=SYSRQ

	revision=$((key_data & 0xff))
	key_count=$(((key_data >> 30) & 0x3))

	for key in "${!key_data_map[@]}"; do
		if ((key_data & key)); then
			key_data_parsed="${key_data_parsed:+$key_data_parsed-}${key_data_map[key]}"
		fi
	done

	while ((key_count_idx++ < key_count)); do
		scan=$(word "${_key_presses[@]::2}")
		key=$(word "${_key_presses[@]:2:2}")
		input_key=""
		if ((scan > 0x0)); then
			input_key=$(scancode "$scan")
		elif ((key > 0x0)); then
			input_key=$(ucs2_to_string "${_key_presses[@]:2:2}" | nprintable)
			[[ $input_key == "." ]] && input_key=$key
		fi

		key_presses+=("$input_key")
		_key_presses=("${_key_presses[@]:4}")
	done

	if ((${#_key_presses[@]} > 0)); then
		if ((${#_key_presses[@]} > 0x48)) && \
			[[ $(platform) == "LENOVO-20L6SBYM0W-ThinkPad T480" ]]; then
			key_presses[-1]+="{$(ucs2_to_string "${_key_presses[@]:0x48}")}"
		else
			key_presses[-1]+="{$(flat_bytes "${_key_presses[@]}")}"
		fi
	fi

	echo "$revision,${key_data_parsed:-N/A},$crc32,$boot_opt,$key_count,${key_presses[*]}"
}

scancode() {
	local code=$1 code_map=()

	code_map[0x00]=null     code_map[0x15]=F11
	code_map[0x01]=UP       code_map[0x16]=F12
	code_map[0x02]=DOWN     code_map[0x48]=Puase
	code_map[0x03]=RIGHT    code_map[0x68]=F13
	code_map[0x04]=LEFT     code_map[0x69]=F14
	code_map[0x05]=Home     code_map[0x6a]=F15
	code_map[0x06]=End      code_map[0x6b]=F16
	code_map[0x07]=Insert   code_map[0x6c]=F17
	code_map[0x08]=Delete   code_map[0x6d]=F18
	code_map[0x09]=PageUp   code_map[0x6e]=F19
	code_map[0x0a]=PageDown code_map[0x6f]=F20
	code_map[0x0b]=F1       code_map[0x70]=F21
	code_map[0x0c]=F2       code_map[0x71]=F22
	code_map[0x0d]=F3       code_map[0x72]=F23
	code_map[0x0e]=F4       code_map[0x73]=F24
	code_map[0x0f]=F5       code_map[0x7f]=Mute
	code_map[0x10]=F6       code_map[0x80]=VolUp
	code_map[0x11]=F7       code_map[0x81]=VolDown
	code_map[0x12]=F8       code_map[0x100]=BrightUp
	code_map[0x13]=F9       code_map[0x101]=BrightDown
	code_map[0x14]=F10      code_map[0x102]=Suspend
	code_map[0x17]=ESC      code_map[0x103]=Hibernate
	                        code_map[0x104]=ToggleDisplay
				code_map[0x105]=Recovery
				code_map[0x106]=Eject

	echo "${code_map[code]:-UNKNOWN}"
}

crc32() {
	# Ripped out of busybox's crc32.c and cksum.c. We care only about the little-endian.

	local data=("$@") byte
	local polynomial=0xedb88320 crc32_table=() crc=0xffffffff
	local val _val

	for ((val = 0; val < 256; val++)); do
		_val=$val idx=8
		while ((idx--)); do
			_val=$((_val & 1 ? ((_val >> 1) ^ polynomial) : (_val >> 1)))
		done
		crc32_table+=("$_val")
	done

	for byte in "${data[@]}"; do
		crc=$((crc32_table[(crc & 0xff) ^ byte] ^ (crc >> 8)))
	done

	plain_hex $(((~crc) & 0xffffffff))
}

parse_efivar_osindications_entry() {
	local data=("$@")
	local osmask bit
	local osbits=() osbits_parsed=()

	osbits[0x01]="boot-to-fw-ui"
	osbits[0x02]="timestamp-revocation"
	osbits[0x04]="file-capsule-delivery"
	osbits[0x08]="fmp-capsule"
	osbits[0x10]="capsule-result-var"
	osbits[0x20]="start-os-recovery"
	osbits[0x40]="start-platform-recovery"
	osbits[0x80]="json-config-data-refresh"

	osmask=$(qword "${data[@]::8}")
	if ((osmask == 0x0)); then
		osbits_parsed+=("none")
	fi
	for bit in "${!osbits[@]}"; do
		if ((osmask & bit)); then
			osbits_parsed+=("${osbits[bit]}")
		fi
	done

	local IFS=","
	echo "${osbits_parsed[*]}"
}

parse_efivar_signature_entry() {
	local data=("$@") _data
	local signatures=()

	_data=("${data[@]}")
	while ((${#_data[@]} > 0)); do
		signatures+=("$(bytes_to_uuid "${_data[@]::16}")")
		_data=("${_data[@]:16}")
	done

	local IFS=","
	echo "${signatures[*]}"
}

parse_efivar_vendorkeys_entry() {
	local data=("$@")
	local vendorkeys_map=()

	vendorkeys_map[0]="modified-by-non-platform"
	vendorkeys_map[1]="modified-by-platform"

	echo "${vendorkeys_map[data[0]]:-UNKNOWN}"
}

parse_efivar_var_error_flag() {
	local data=("$@")
	local error_map

	error_map[0xef]="system-error"
	error_map[0xfe]="user-error"
	error_map[0xff]="no-error"

	echo "${error_map[data[0]]:-UNKNOWN}"
}

format_gen_device() {
	local type=$1 subtype=$2 data=("${@:3}")
	local subtype_desc="" filepath=""

	case $((type)):$((subtype)) in
		1:1)
			subtype_desc=Pci
			filepath="${data[1]},${data[0]}"
			;;
		1:4)
			subtype_desc=Vendor
			filepath=$(bytes_to_uuid "${data[@]::16}")
			if ((${#data[@]} > 16)); then
				filepath+=",$(flat_bytes "${data[@]:16}")"
			fi
			;;
		2:1)
			local hid uid code product
			subtype_desc=ACPI
			code=$(word_l "${data[@]::2}")
			product=$(word "${data[@]:2:2}")
			uid=$(dword "${data[@]:4:4}")
			hid=$(build_textid "$code" "$product")

			if [[ $hid == PNP0A03 ]]; then
				subtype_desc=PciRoot
				filepath=$uid
			else
				filepath="$hid,$uid"
			fi
			;;
		2:3)
			subtype_desc=_ADR
			local _data=("${data[@]}")
			while ((${#_data[@]} > 0)); do
				filepath=${filepath:+$filepath;}$(format_adr "$(dword "${_data[@]::4}")")
				_data=("${_data[@]:4}")
			done
			;;
	esac
	echo "$subtype_desc($filepath)"
}

format_adr() {
	local adr=$1 desc=""

	local type=() fw_detect=() non_vga=()
	type[0]=Other
	type[1]=VGA/CRT/VESA
	type[2]=TV/HDTV
	type[3]=external-digital-monitor
	type[4]=digital-flat-panel

	fw_detect[0]=fw-cannot-detect
	fw_detect[1]=fw-can-detect

	non_vga[0]=VGA
	non_vga[1]=non-VGA

	if ((!(adr & 1 << 31))); then
		echo "$adr"
		return 0
	fi

	# Follow the ACPI's Table B-2: Video Output Device Attributes
	desc+=$((adr & 0x1f)):$(((adr >> 4) & 0xf))
	desc+=",${type[(adr >> 8) & 0xf]:-UNKNOWN}"
	desc+=",${fw_detect[adr & 1 << 16 ? 1 : 0]}"
	desc+=",${non_vga[adr & 1 << 17 ? 1 : 0]}"
	desc+=",$(((adr >> 18) & 0x7))"
	if ((debug == 1)); then
		desc+=",($adr)"
	fi

	echo "$desc"
}

format_msg_device() {
	local subtype=$1 data=("${@:2}")
	local subtype_desc="" filepath=""

	case $((subtype)) in
		5)
			subtype=USB
			filepath="${data[0]},${data[1]}"
			;;
		10)
			subtype_desc=VenMsg
			filepath=$(bytes_to_uuid "${data[@]::16}")
			if ((${#data[@]} > 16)); then
				filepath+=",$(flat_bytes "${data[@]:16}")"
			fi
			;;
		11)
			subtype_desc=MAC
			# MAC is actually 32 bytes long in this struct, but the rest is
			# padded with zeroes so we extract just the relevant bytes.
			filepath=$(flat_bytes "${data[@]::6}")
			filepath+=",$(dec_efivar "${data[32]}")"
			;;
		12)
			local type=([0]=DHCP [1]=static)
			subtype_desc=IPv4
			filepath=$(format_ipv4 "${data[@]::4}")
			filepath+=",$(format_ipv4 "${data[@]:4:4}")"
			filepath+=",$(dec_efivar "$(word "${data[@]:8:2}")")"
			filepath+=",$(dec_efivar "$(word "${data[@]:10:2}")")"
			filepath+=",$(dec_efivar "$(word "${data[@]:12:2}")")"
			filepath+=",${type[data[14]]:-UNKNOWN}"
			filepath+=",$(format_ipv4 "${data[@]:15:4}")"
			filepath+=",$(format_ipv4 "${data[@]:19:4}")"
			;;
		13)
			local type=([0]=static [1]=stateless [2]=stateful)
			subtype_desc=IPv6
			filepath=$(format_ipv6 "${data[@]::16}")
			filepath+=",$(format_ipv6 "${data[@]:16:16}")"
			filepath+=",$(dec_efivar "$(word "${data[@]:32:2}")")"
			filepath+=",$(dec_efivar "$(word "${data[@]:34:2}")")"
			filepath+=",$(dec_efivar "$(word "${data[@]:36:2}")")"
			filepath+=",${type[data[38]]:-UNKNOWN}"
			filepath+=",$(dec_efivar "${data[39]}")"
			filepath+=",$(format_ipv6 "${data[@]:40:16}")"
			;;
		14)
			local default=([0]=default)
			local parity=(
				[0]=default
				[1]=no-parity
				[2]=even-parity
				[3]=odd-parity
				[4]=mark-parity
				[5]=space-parity
			)
			local stop_bits=(
				[0]=default
				[1]=1bit
				[2]=1.5bit
				[3]=2bit
			)
			local baud_rate data_bits

			subtype_desc=UART
			baud_rate=$(dec_efivar "$(qword "${data[@]:4:8}")")
			data_bits=$(dec_efivar "${data[12]}")

			filepath=${default[baud_rate]:-$baud_rate}
			filepath+=",$data_bits,${parity[data[13]]},${stop_bits[data[14]]}"
			;;
		15)
			subtype_desc=USBClass
			filepath=$(word "${data[@]::2}")
			filepath+=",$(word "${data[@]:2:2}")"
			filepath+=",${data[4]},${data[5]},${data[6]}"
			;;
		18)
			subtype_desc=SATA
			filepath=$(dec_efivar "$(word "${data[@]::2}")")
			filepath+=",$(dec_efivar "$(word "${data[@]:2:2}")")"
			filepath+=",$(dec_efivar "$(word "${data[@]:4:2}")")"
			;;
		23)
			local ns eui
			subtype_desc=NVMe
			ns=$(dword "${data[@]::4}")
			eui=$(format_eui "${data[@]:4:8}")

			filepath="$ns,$eui"
			;;
		24)
			subtype_desc=Uri
			if ((${#data[@]} > 0)); then
				filepath=$(bytes_to_string "${data[@]}")
			fi
			;;
	esac
	echo "$subtype_desc($filepath)"
}

format_media_type() {
	local subtype=$1 data=("${@:2}")
	local subtype_desc="" filepath=""

	case $((subtype)) in
		1)
			local part_number start size signature_type format signatures desc
			subtype_desc=HD

			signatures[0x1]=$(dword "${data[@]:20:4}")
			signatures[0x2]=$(bytes_to_uuid "${data[@]:20:16}")

			desc[0x1]=MBR
			desc[0x2]=GPT

			part_number=$(dword "${data[@]::4}")
			start=$(qword "${data[@]:4:8}")
			size=$(qword "${data[@]:12:8}")
			format=${data[36]}
			signature_type=${data[37]}

			filepath+="$part_number"
			filepath+=",${desc[format]:-UNKNOWN}"
			filepath+=",${signatures[signature_type]:-UNKNOWN}"
			filepath+=",$size"
			filepath+=",$start"
			;;
		3)
			subtype_desc=VenMedia
			filepath=$(bytes_to_uuid "${data[@]::16}")
			;;
		4)
			subtype_desc=File
			filepath=$(ucs2_to_string "${data[@]}")
			;;
		6)
			subtype_desc=FvFile
			;;&
		7)
			subtype_desc=FvVol
			;;&
		6 | 7)
			filepath=$(bytes_to_uuid "${data[@]::16}")
			;;
	esac
	echo "$subtype_desc($filepath)"
}

format_bbs_type() {
	local subtype=$1 data=("${@:2}")
	local subtype_desc="" filepath=""

	case $((subtype)) in
		1)
			subtype_desc=BBS
			local devtype status description

			local devtype_map=(
				[0x01]=Floppy
				[0x02]=HD
				[0x03]=CD-ROM
				[0x04]=PCMCIA
				[0x05]=USB
				[0x06]=Embedded-network
				[0x80]=BEV
				[0xff]=UNKNOWN
			)

			devtype=$(word "${data[@]::2}")
			# See BIOS Boot Specfor details
			status=$(word "${data[@]:2:2}")
			description=$(bytes_to_string "${data[@]:4}")

			filepath="${devtype_map[devtype]},$status"
			[[ -n $description ]] && filepath+=",$description"
			;;
	esac
	echo "$subtype_desc($filepath)"
}

format_eui() {
	local bytes=("$@") IFS="-"

	((${#bytes[@]} == 8)) || return 1

	bytes=("${bytes[@]/0x/}")
	echo "${bytes[*]}"
}

format_ipv4() {
	local bytes=("$@")

	((${#bytes[@]} == 4)) || return 1

	printf '%u.%u.%u.%u' "${bytes[@]}"
}

format_ipv6() {
	# Keep it simple
	local bytes=("$@") words=() word_idx word

	((${#bytes[@]} == 16)) || return 1
	for ((word_idx = 0; word_idx < 16; word_idx += 2)); do
		word=$(word_l "${bytes[@]:word_idx:2}")
		# Prep for squashing done at the very end
		((word == 0x0000)) && word=""
		words+=("${word/0x/}")
	done

	local IFS=":"
	ipv6="${words[*]}"
	# Squash it
	ipv6=${ipv6//+(:+(:))/::}

	echo "[$ipv6]"
}

raw_data() {
	local data=("$@") _data idx

	for idx in "${!data[@]}"; do
		_data+="${data[idx]}"
		if ((idx < ${#data[@]} - 1 && (idx + 1) % 16 == 0)); then
			_data+=$'\n'
		elif ((idx < ${#data[@]} - 1 && (idx + 1) % 8 == 0)); then
			_data+="  "
		else
			_data+=" "
		fi
	done

	echo "$_data"
}

detect_guids() {
	local guid=$1
	local -A guids=()

	# Format here follows flat_bytes()
	guids["$NULL_GUID"]=null_guid
	guids["$EFI_GLOBAL_VARIABLE_GUID"]=global_efivars_guid
	guids["$ADDRESS_RANGE_MIRROR_VARIABLE_GUID"]=address_range_mirror
	guids["$EFI_LOADER_ENTRY_GUID"]=loader_efivars_guid
	guids["$EFI_SHIM_LOCK_GUID"]=shim_efivars_guid
	guids["$EFI_AUTO_CREATED_GUID"]=auto_created_boot_option
	guids["a5c059a1-94e4-4aa7-87b5-ab155c2bf072"]=cert_x509
	guids["668f4529-63d0-4bb5-b65d-6fbb9d36a44a"]=qemu_bootorder
	guids["04b37fe8-f6ae-480b-bdd5-37d98c5e89aa"]=edk_var_error_flag
	guids["eb704011-1402-11d3-8e77-00a0c969723b"]=mtc_vendor

	if [[ -n $guid && -n ${guids["$guid"]} ]]; then
		echo "${guids["$guid"]}"
	elif [[ $guid == list ]]; then
		for guid in "${!guids[@]}"; do
			echo "$guid (${guids["$guid"]})"
		done
	elif [[ -n $guid ]]; then
		# shellcheck disable=SC2046
		flat_bytes $(uuid_to_bytes "$guid")
	fi
}

decompress_eisa() {
	local ceisa=$1 deisa=()

	# Taken verbatim from the ACPI spec:

	# The algorithm used to convert the TextID is as shown in the following example:

	# Starting with a seven character input string “PNP0303”, we want to create a DWordConst.
	# This string contains a three character manufacturer code “PNP”, a three character hex
	# product identifier “030”, and a one character revision identifier “3”.

	# The compressed manufacturer code is created as follows:

	# Find hex ASCII value for each letter
	# Subtract 40h from each ASCII value
	# Retain 5 least significant bits for each letter and discard remaining 0’s:

	# Byte 0:
	# Bit 7: reserved (0)
	# Bit 6-2: 1st character of compressed mfg code “P”
	# Bit 1-0: Upper 2 bits of 2nd character of mfg code “N”
	# Byte 1:
	# Bit 7-5: Lower 3 bits of 2nd character of mfg code “N”
	# Bit 4-0: 3rd character of mfg code “P”
	# Byte 2:
	# Bit 7-4: 1st hex digit of product number “0”
	# Bit 3-0: 2nd hex digit of product number “3”
	# Byte 3:
	# Bit 7-4: 3rd hex digit of product number “0”
	# Bit 3-0: 4th hex digit of product number “3”

	# Here we reverse the above to decompress the manufacturer code
	deisa+=("$(plain_hex $((((ceisa & 0xff) >> 2) + 0x40)))")
	deisa+=("$(plain_hex $(((((ceisa & 0x3) << 3) | (((ceisa >> 8) >> 5) & 0x7)) + 0x40)))")
	deisa+=("$(plain_hex $((((ceisa >> 8) & 0x1f) + 0x40)))")

	bytes_to_string "${deisa[@]}"
}

build_textid() {
	local code=$1 product=$2 textid=""

	# See decompress_eisa for explanation how these value are encoded
	textid=$(decompress_eisa "$code")$(hex_efivar "$product")

	[[ -n $textid ]] || return 1
	echo "$textid"
}

parse_efivar_attributes() {
	local attributes=("$@") attributes_map=() attribute
	local attributes_mask=0x0 attributes_parsed

	attributes_map[0x01]=non-volatile
	attributes_map[0x02]=bootservice-access
	attributes_map[0x04]=runtime-access
	attributes_map[0x08]=hw-error-record
	attributes_map[0x10]=auth-write-access
	attributes_map[0x20]=time-based-auth-write-access
	attributes_map[0x40]=append-write

	attributes_mask=$(dword "${attributes[@]}")

	for attribute in "${!attributes_map[@]}"; do
		((attributes_mask & attribute)) || continue
		attributes_parsed+=("${attributes_map[attribute]}")
	done

	local IFS=","
	echo "${attributes_parsed[*]}"
}

get_valid_boot_id() {
	local bootid=${1,,} _bootid
	local bootid_valid_name boot_efivar

	bootid=${bootid//+(boot|0x)/}

	[[ $bootid =~ ^[0-9a-f]+$ ]] || return 1
	_bootid=$(hex_efivar "0x$bootid")
	bootid_valid_name=Boot$_bootid

	boot_efivar=$(get_efivars_user "$bootid_valid_name")
	[[ -n $boot_efivar ]] || return 1

	echo "$_bootid" "$boot_efivar"

}

_set_activity() {
	local bootopt_efivar_file=$1 activity=$2 bootopt_activity_data=()

	# shellcheck disable=SC2207
	bootopt_activity_data=($(hex "$bootopt_efivar_file"))
	bootopt_activity_data[4]=$(plain_hex $(((bootopt_activity_data[4] & ~1) | activity)))

	write_efivar "$bootopt_efivar_file" 1 "${bootopt_activity_data[@]}" || return 1
	parse_efivars "$bootopt_efivar_file"
}

set_activity() {
	local bootopt=$1 activity=$2 bootopt_efivar_file

	read -r _ bootopt_efivar_file < <(get_valid_boot_id "$bootopt") || return 1
	_set_activity "$bootopt_efivar_file" "$activity"
}

_set_boot_next() {
	local bootopt=${1,,} bootopt_id _bootopt
	local bootopt_efivar="" bootopt_data=""
	local boot_order_data="" boot_order_list=()

	# We need to verify if given bootopt actually exists. If it does,
	# we then check if it's on the BootOrder list. If both conditions
	# check out, we create the efivar.
	read -r bootopt_id bootopt_efivar < <(get_valid_boot_id "$bootopt") || return 1

	bootopt_data=$(parse_efivars "$bootopt_efivar")
	# Bail if efivar is not valid and not active
	[[ $bootopt_data == "Boot$bootopt_id "*"(*"* ]] || return 1

	# shellcheck disable=SC2207
	boot_order_list=($(get_boot_order_list)) || return 1

	for _bootopt in "${boot_order_list[@]}"; do
		if [[ $_bootopt == "$bootopt_id" ]]; then
			set_boot_next "0x$bootopt" "$bootopt_data" || return 1
			return 0
		fi
	done

	return 1
}

get_boot_order_list() {
	local boot_order_data boot_order_list

	boot_order_data=$(parse_efivars "$EFIVARS/BootOrder-$EFI_GLOBAL_VARIABLE_GUID")
	# Bail if BootOrder is invalid
	[[ -n $boot_order_data ]] || return 1

	IFS="," read -ra boot_order_list <<<"${boot_order_data#*: }"
	printf '%s\n' "${boot_order_list[@]}"
}

set_boot_next() {
	local bootopt=$1 bootopt_data=$2
	local boot_next_efivar_data=()
	local boot_next_efivar_file=$EFIVARS/BootNext-$EFI_GLOBAL_VARIABLE_GUID

	# All data is kept in little-endian format
	boot_next_efivar_data+=(0x7 0x0 0x0 0x0) # var attributes
	# data
	boot_next_efivar_data+=("$(plain_hex $((bootopt & 0xff)))")
	boot_next_efivar_data+=("$(plain_hex $(((bootopt & 0xff00) >> 8)))")

	write_efivar "$boot_next_efivar_file" 1 "${boot_next_efivar_data[@]}" || return 1

	parse_efivars "$boot_next_efivar_file"
	echo "$bootopt_data"
}

set_boot_order() {
	local bootorder=$1 bootorder_list=()
	local bootorder_efivar_data=()
	local bootorder_efivar_file=$EFIVARS/BootOrder-$EFI_GLOBAL_VARIABLE_GUID

	local bootopt boot_id boot_efivar
	local bootopt_data=()
	local -A bootopt_dups=()

	IFS="," read -ra bootorder_list <<<"$bootorder"

	for bootopt in "${bootorder_list[@]}"; do
		if read -r boot_id boot_efivar; then
			[[ -z ${bootopt_dups["$boot_id"]} ]] || continue
			bootopt_data+=("0x$boot_id") bootopt_dups["$boot_id"]=1
		else
			return 1
		fi < <(get_valid_boot_id "$bootopt")
	done

	# All data is kept in little-endian format
	bootorder_efivar_data+=(0x7 0x0 0x0 0x0) # var attributes
	# data
	for bootopt in "${bootopt_data[@]}"; do
		bootorder_efivar_data+=(
			"$(plain_hex $((bootopt & 0xff)))"
			"$(plain_hex $(((bootopt & 0xff00) >> 8)))"
		)
	done

	write_efivar "$bootorder_efivar_file" 1 "${bootorder_efivar_data[@]}" || return 1
	parse_efivars "$bootorder_efivar_file"
}

set_timeout() {
	local timeout=${1,,}
	local timeout_efivar_data=()
	local timeout_efivar_file=$EFIVARS/Timeout-$EFI_GLOBAL_VARIABLE_GUID

	timeout=${timeout/0x/}
	[[ $timeout =~ ^[0-9a-f]+$ ]] || return 1
	timeout=0x$timeout

	if ((timeout < 0x0 || timeout > 0xffff)); then
		return 1
	fi

	# All data is kept in little-endian format
	timeout_efivar_data+=(0x7 0x0 0x0 0x0) # var attributes
	# data
	timeout_efivar_data+=("$(plain_hex $((timeout & 0xff)))")
	timeout_efivar_data+=("$(plain_hex $(((timeout & 0xff00) >> 8)))")

	write_efivar "$timeout_efivar_file" 1 "${timeout_efivar_data[@]}" || return 1
	parse_efivars "$timeout_efivar_file"
}

set_bpath() {
	local bpath=$1

	case "$bpath_type" in
		disk) set_bpath_disk "$bpath" ;;
		net) set_bpath_net "$bpath" ;;
		*) return 1 ;;
	esac
}

set_bpath_disk() {
	# TODO: We support only the short HD() path for now for GPT-capable drives
	local bpath=$1

	local id
	local disk
	local label
	local partition_id
	local part_uuid
	local loader
	local efivar_file


	local partition partition_dev
	local part_start
	local part_size
	local mount part_mount fs

	local bpath_data
	local filepath_data
	local description_data

	IFS=":" read -r id disk partition_id loader label <<<"$bpath"

	[[ -z $id ]] && id=0x42
	[[ -z $label ]] && label=Linux
	[[ -z $partition_id ]] && partition_id=1

	id=$(hex_efivar "$id")
	efivar_file=$EFIVARS/Boot$id-$EFI_GLOBAL_VARIABLE_GUID

	disk=/dev/${disk##*/}
	[[ -b $disk ]] || return 1
	[[ -f $loader ]] || return 1

	# We support only GPT drives
	# shellcheck disable=SC2046
	[[ $(bytes_to_string $(hex -s512 -n8 "$disk")) == "EFI PART" ]] || return 1

	for partition in "/sys/block/${disk##*/}/${disk##*/}"*; do
		[[ -e $partition/partition ]] || continue
		if (($(< "$partition/partition") == partition_id)); then
			partition_dev=${partition##*/}
		fi
	done

	[[ -b /dev/$partition_dev ]] || return 1
	part_start=$(< "/sys/class/block/$partition_dev/start")
	part_size=$(< "/sys/class/block/$partition_dev/size")
	part_uuid=$(blkid -s PARTUUID -o value "/dev/$partition_dev")

	while read -r partition mount fs _; do
		if [[ $partition == "/dev/$partition_dev" && $fs == vfat ]]; then
			part_mount=$mount
		fi
	done </proc/mounts

	[[ -d $part_mount ]] || return 1
	loader=${loader#"$part_mount"}
	# Convert to FAT-based path
	loader=${loader//\//\\}

	# Now we have all the data, we start parsing them into actual Boot####
	# All data is kept in little-endian format
	# shellcheck disable=SC2207
	description_data+=($(string_to_ucs2 "$label") 0x0 0x0) # description

	filepath_data+=(0x4 0x1 0x2a 0x0) # Media Device Path (HD), size 42 bytes
	# shellcheck disable=SC2207
	filepath_data+=($(val_to_bytes "$partition_id" 4)) # partition number
	# shellcheck disable=SC2207
	filepath_data+=($(val_to_bytes "$part_start" 8)) # partition start
	# shellcheck disable=SC2207
	filepath_data+=($(val_to_bytes "$part_size" 8)) # partition size
	# shellcheck disable=SC2207
	filepath_data+=($(uuid_to_bytes "$part_uuid")) # partition signature
	filepath_data+=(0x2) # partition format - GPT
	filepath_data+=(0x2) # signature type - GUID
	filepath_data+=(0x4 0x4) # Media Device Path (File) - this where we put our loader
	# shellcheck disable=SC2207
	filepath_data+=($(val_to_bytes $((4 + ${#loader} * 2 + 2)) 2)) # Size of the File
	# shellcheck disable=SC2207
	filepath_data+=($(string_to_ucs2 "$loader") 0x0 0x0)
	filepath_data+=(0x7f 0xff 0x4 0x0) # End Device Path

	bpath_data+=(0x7 0x0 0x0 0x0) # var attributes
	bpath_data+=(0x1 0x0 0x0 0x0) # Boot#### attributes
	# shellcheck disable=SC2207
	bpath_data+=($(val_to_bytes "${#filepath_data[@]}" 2)) # file_path_list_length
	bpath_data+=("${description_data[@]}")
	bpath_data+=("${filepath_data[@]}")

	write_efivar "$efivar_file" 1 "${bpath_data[@]}" || return 1
	parse_efivars "$efivar_file"
}

set_bpath_net() {
	# TODO: We support only devices attached directly to the pci bus. efibootmgr
	# creates net boot entries only with MAC()/IPv4 paths which doesn't seem to
	# be enough for some of the fw (e.g. OVMF) so we create path starting at the
	# PciRoot(). This relies on proper mapping under the acpi bus so there's no
	# guarantee it's bulletproof.
	local bpath=$1

	local id
	local net_dev
	local protocol
	local uri
	local uri_protocol
	local label

	local mac
	local pci_path
	local -A protocols=() protocols_l=()


	local bpath_data
	local filepath_data
	local description_data
	local pci_data

	protocols["ipv4"]=0xc
	protocols["ipv6"]=0xd
	protocols_l["ipv4"]=0x1b
	protocols_l["ipv6"]=0x3c

	IFS=":" read -r id net_dev protocol uri uri_protocol label <<<"$bpath"

	[[ -z $id ]] && id=0x43
	[[ -z $protocol ]] && protocol=ipv4
	[[ -z $uri_protocol ]] && uri_protocol=http
	[[ -z $label ]] && label="Linux PXE"

	id=$(hex_efivar "$id")
	efivar_file=$EFIVARS/Boot$id-$EFI_GLOBAL_VARIABLE_GUID

	net_dev=${net_dev,,} protocol=${protocol,,} uri_protocol=${uri_protocol,,}
	[[ -e /sys/class/net/$net_dev/address ]] || return 1
	(($(< "/sys/class/net/$net_dev/type") == 0x1)) || return 1

	# shellcheck disable=SC2207
	mac=($(mac_to_bytes "$(< "/sys/class/net/$net_dev/address")"))
	pci_path=$(readlink -f "/sys/class/net/$net_dev/device")
	# The device link can also point at a virtio device, so resolve it to the actual pci address
	pci_path=$(virtio_to_pci "$pci_path")

	# Now we have all the data, we start parsing them into actual Boot####
	# All data is kept in little-endian format
	# shellcheck disable=SC2207
	description_data+=($(string_to_ucs2 "$label (MAC:$(flat_bytes "${mac[@]}"))") 0x0 0x0) # description

	# shellcheck disable=SC2207
	filepath_data+=($(get_pci_data "$pci_path")) # ACPI Device Path/PCI Device Path

	filepath_data+=(0x3 0xb 0x25 0x0) # Messaging Device Path (MAC), 37 bytes
	# shellcheck disable=SC2207
	filepath_data+=("${mac[@]}" $(zeroed 26)) # MAC addresses, padded with 0s to fit 32 bytes
	filepath_data+=(0x1) # Network interface type. See sysfs check above

	# Messaging Device Path IPv4 or IPv6, size 27 bytes or 60 bytes respectively
	# shellcheck disable=SC2207
	filepath_data+=(0x3 "${protocols["$protocol"]}" $(val_to_bytes "${protocols_l["$protocol"]}" 2))
	# For now, we simply fill the struct with all 0x0s to indicate default settings. TODO: support
	# custom setup, with different IPs, protocols, etc.
	# shellcheck disable=SC2207
	case "$protocol" in
		ipv4) filepath_data+=($(zeroed 23)) ;; # see size above
		ipv6)
			# We need to split it since the prefix length needs to be != 0x0
			filepath_data+=($(zeroed 39))
			filepath_data+=(0x40) # prefix length (/64)
			filepath_data+=($(zeroed 16))
			;;
	esac

	# Messaging Device Path (URI), size 4 bytes + n
	if [[ -n $uri ]]; then
		# Spec says that the URI can be empty, hence have a special handler for such a case.
		if [[ $uri == empty ]]; then
			uri=""
		else
			uri=${uri_protocol}://$uri
		fi
		# shellcheck disable=SC2207
		filepath_data+=(0x3 0x18 $(val_to_bytes $((0x4 + ${#uri})) 2))
		# shellcheck disable=SC2207
		filepath_data+=($(string_to_bytes "$uri"))
	fi

	filepath_data+=(0x7f 0xff 0x4 0x0) # End Device Path

	bpath_data+=(0x7 0x0 0x0 0x0) # var attributes
	bpath_data+=(0x1 0x0 0x0 0x0) # Boot#### attributes
	# shellcheck disable=SC2207
	bpath_data+=($(val_to_bytes "${#filepath_data[@]}" 2)) # file_path_list_length
	bpath_data+=("${description_data[@]}")
	bpath_data+=("${filepath_data[@]}")

	write_efivar "$efivar_file" 1 "${bpath_data[@]}" || return 1
	parse_efivars "$efivar_file"
}

delete_boot_opt() {
	local bootopt=$1 bootopt_efivar_file bootopt_id _bootopt
	local bootorder new_bootorder

	read -r bootopt_id bootopt_efivar_file < <(get_valid_boot_id "$bootopt") || return 1
	unlink_efivar "$bootopt_efivar_file" 1 || return 1

	# shellcheck disable=SC2207
	bootorder=($(get_boot_order_list)) || return 0

	for _bootopt in "${bootorder[@]}"; do
		[[ $_bootopt == "$bootopt_id" ]] && continue
		new_bootorder=${new_bootorder:+$new_bootorder,}$_bootopt
	done

	[[ -n $new_bootorder ]] || return 0

	set_boot_order "$new_bootorder"
}

zeroed() {
	local no=$1

	no=$((no <= 0 ? 1 : no))
	# This is way faster than having a plain loop - for small number of repeated
	# chars the difference is quite small but going above 10k range there's almost
	# 100% degredation in speed in favor of this little trick.
	eval "printf '0x0%.0s\n' {0..$((no - 1))}"
}

get_pci_dev_func() {
	local pci=$1

	[[ $pci =~ [a-f0-9]{4}:[a-f0-9]{2}:([a-f0-9]{2}).([a-f0-9]) ]] || return 1
	# We return it in order of PCI Device Path, i.e. function first
	printf '%s\n' "0x${BASH_REMATCH[2]}" "0x${BASH_REMATCH[1]}"

}

get_pci_data() {
	# Convert sysfs pci path into data representing device and function of each
	# pci device on either the pci or pcie bus.

	local pci_path=$1
	local pci_devs=() pci_dev
	local pci_data=() hid uid

	pci_path=${pci_path#/sys/devices/}
	IFS="/" read -ra pci_devs <<<"$pci_path"

	((${#pci_devs[@]} > 0)) || return 1
	[[ -e /sys/devices/${pci_devs[0]}/firmware_node/hid ]] || return 1
	[[ -e /sys/devices/${pci_devs[0]}/firmware_node/uid ]] || return 1

	hid=$(< "/sys/devices/${pci_devs[0]}/firmware_node/hid")
	uid=$(< "/sys/devices/${pci_devs[0]}/firmware_node/uid")
	# pci bus (PNP0A03) or pcie bus (PNP0A08), we refuse to work with anything else
	[[ $hid == PNP0A03 || $hid == PNP0A08 ]] || return 1

	pci_data+=(0x2 0x1 0xc 0x0) # ACPI Device Path, size 12 bytes
	pci_data+=(0xd0 0x41 0x3 0x0a) # _HID (We use ONLY PNP0A03 for compatibility)
	# FIXME: Under some platforms there may be a mismatch between this _UID and what was
	# reported by the {auto_created_boot_option} efivar. Buggy fw or can it really be
	# misidentified?
	# shellcheck disable=SC2207
	pci_data+=($(val_to_bytes "$uid" 4)) # _UID

	for pci_dev in "${pci_devs[@]:1}"; do
		pci_data+=(0x1 0x1 0x6 0x0) # Hardware Device Path (PCI), size 6 bytes
		# shellcheck disable=SC2207
		pci_data+=($(get_pci_dev_func "$pci_dev")) || return 1
	done

	printf '%s\n' "${pci_data[@]}"
}

virtio_to_pci() {
	local dev=$1

	[[ ${dev##*/} == virtio* ]] && dev=${dev%/*}
	echo "$dev"
}

mac_to_bytes() {
	local _mac

	IFS=":" read -ra _mac <<<"$1"
	printf '%s\n' "${_mac[@]/#/0x}"
}

write_efivar() {
	local efivar=$1 valid=$2 data=("${@:3}")

	unlink_efivar "$efivar" "$valid"
	to_bin "${data[@]}" > "$efivar" || return 1
}

unlink_efivar() {
	local efivar=$1 valid=$2

	if [[ -f $efivar ]]; then
		# Some efivars are validated by the kernel, hence if they already exist
		# they should not have the immutable flag set. Caller is responsible for
		# providing info if given $efivar is valid (mutable) or not (immutable).
		if ((valid == 0)); then
			chattr -i "$efivar"
		fi
		unlink "$efivar" || return 1
	fi
}

to_bin() {
	# Caller must make sure that this list holds hex values
	local bytes=("$@")

	bytes=("${bytes[@]/0x/}")
	bytes=("${bytes[@]/#/\\x}")

	local IFS=""
	printf '%b' "${bytes[*]}" | cat
}

bytes_to_val() {
	local length=$1 bytes=("${@:2}") byte
	local val=0x0

	((length > 0 && length <= 8)) || return 1

	for ((byte = 0; byte < length; byte++)); do
		: $((val |= bytes[byte] << (byte * 8)))
	done
	printf '0x%x\n' "$val"
}

val_to_bytes() {
	local val=$1 length=${2:-8} shift
	local bytes=()

	for ((shift = 0; shift < length; shift++)); do
		bytes+=("$(plain_hex $(((val >> (shift * 8)) & 0xff)))")
	done

	printf '%s\n' "${bytes[@]}"
}

bytes_to_val_l() {
	local length=$1 bytes=("${@:2}") byte byte_idx
	local val=0x0

	((length > 0 && length <= 8)) || return 1

	for ((byte = length - 1, byte_idx = 0; byte >= 0; byte--, byte_idx++)); do
		: $((val |= bytes[byte_idx] << (byte * 8)))
	done

	printf '0x%x\n' "$val"
}

val_to_bytes_l() {
	local val=$1 length=${2:-8} shift idx
	local bytes=()

	for ((shift = length - 1, idx = 0; idx < length; shift--, idx++)); do
		bytes+=("$(plain_hex $(((val >> (shift * 8)) & 0xff)))")
	done

	printf '%s\n' "${bytes[@]}"
}

flat_bytes() {
	local bytes=("$@") IFS=""

	bytes=("${bytes[@]/0x/}")

	echo "${bytes[*]}"
}

ucs2_to_string() {
	local data=("$@") _data
	local offset=0 string="" char

	while _data=("${data[@]:offset:2}"); do
		# Check fur NUL and bail if we reached it
		if ((_data[0] == 0x0 && _data[1] == 0x0)); then
			break
		fi
		char="\u${_data[1]/0x}${_data[0]/0x}"
		string+="$char"
		((offset += 2))
	done

	printf '%b' "$string"
}

ucs2_to_string_list() {
	local data=("$@") string list

	while ((${#data[@]} > 0)); do
		string=$(ucs2_to_string "${data[@]}")
		list=${list:+$list,}$string
		data=("${data[@]:${#string} * 2 + 2}")
	done

	echo "$list"
}

string_to_ucs2() {
	local string=$1 ucs2=()
	local char

	for ((char = 0; char < ${#string}; char++)); do
		ucs2+=("$(plain_hex "'${string:char:1}")" 0x0)
	done

	printf '%s\n' "${ucs2[@]}"
}

bytes_to_string() {
	local bytes=("$@") byte
	local string=""

	for byte in "${bytes[@]}"; do
		((byte == 0x0)) && break
		string+=$(printf '%b' "\x${byte/0x/}")
	done

	echo "$string"
}

string_to_bytes() {
	local string=$1 bytes=()
	local char

	for ((char = 0; char < ${#string}; char++)); do
		bytes+=("$(plain_hex "'${string:char:1}")")
	done

	printf '%s\n' "${bytes[@]}"
}

list_efivars() {
	local efivars=("$@")

	efivars=("${efivars[@]##*/}")
	efivars=("${efivars[@]%-*-*-*-*-*}")

	printf '%s\n' "${efivars[@]}" | sort
}

get_efivars_user() {
	local efivar=$1 efivars_user=()

	if [[ -f $efivar ]]; then
		echo "$efivar"
	elif [[ -n $efivar ]]; then
		efivars_user=("$EFIVARS/$efivar"*)
		if ((${#efivars_user[@]} > 0)); then
			printf '%s\n' "${efivars_user[@]}"
		fi
	fi
}

handle_setters() {
	local setter=$1

	case "$setter" in
		bactivity_0) activity=0 ;;&
		bactivity_1) activity=1 ;;&
		bactivity*)  set_activity "${setters["$setter"]}" "$activity" ;;
		bnext)       _set_boot_next "${setters["$setter"]}" ;;
		border)      set_boot_order "${setters["$setter"]}" ;;
		timeout)     set_timeout "${setters["$setter"]}" ;;
		bpath)       set_bpath "${setters["$setter"]}" ;;
		dboot)       delete_boot_opt "${setters["$setter"]}" ;;
		*)           return "${setters_es["unsupported"]}" ;;
	esac
}

isroot() { ((UID == 0)); }

qword() { bytes_to_val 8 "$@"; }
dword() { bytes_to_val 4 "$@"; }
word() { bytes_to_val  2 "$@"; }
abytes() { bytes_to_val "$1" "${@:2}"; }

qword_l() { bytes_to_val_l 8 "$@"; }
dword_l() { bytes_to_val_l 4 "$@"; }
word_l() { bytes_to_val_l  2 "$@"; }
abytes_l() { bytes_to_val_l "$1" "${@:2}"; }

help() {
	cat <<-HELP

		${0##*/}: [-[Aa] Boot####] [-b bootorder] [-B] [-d] [-f efivar] [-ghlL] [-G GUID] [-n Boot####] [-p bpath] [-P bpath_type] [-t timeout] [-x Boot####]

		  -a - Set Boot#### as active.
		  -A - set Boot#### as inactive.
		  -B - Process only Boot#### efivars.
		  -b - Set BootOrder to bootorder. bootorder is a comma-separated list of
		       valid Boot* options.
		  -d - include raw hex dump of given efivar.
		  -f efivar - parse given efivar. it can be either a file or a matching
		              efivar name.
		  -g - process only global efivars. Shortcut for -G 8be4df61-93ca-11d2-aa0d-00e098032b8c.
		  -G - process only efivars matching GUID.
		  -l - list present efivars.
		  -L - List all known GUIDs.
		  -h - print this help.
		  -n - Set BootNext to Boot#### option.
		  -p - Create Boot#### based on bpath. bpath should be a colon-separated list
		       of attributes describing Boot#### efivar. The format is dependent on
		       the selected bpath_type:

		       disk:
		         id:disk:partition_id:loader:label

		         id - id for the Boot#### efivar. 0x42 (0042) by default.
		         disk - path to a block device representing a drive to boot from. Must be
		              provided.
		         partition_id - partition id where the loader is located. 1 by default.
		         loader - path to EFI loader. Must be provided.
		         label - description of the new Boot#### efivar. 'Linux' by default.

		       net:
		         id:net_dev:protocol:uri:uri_protocol:label

		         id - as per above. 0x43 (0043) by default.
		         net_dev - name of the net device to use as per net subsystem (e.g. eth0).
		                   Must be provided.
		         protocol - Which IP protocol to use. ipv4 by default.
		         uri - URI path. It can be used for http boot for instance.
		         uri_protocol: protocol for URI. http by default.
		         label - as per above. 'Linux PXE' by default.

		  -P - Type of bpath passed via -p. disk by default. Supported values:
		         disk, net
		  -t - Set boot manager Timeout to timeout. Value is treated as hex.
		  -x - Delete Boot#### - upon successful deletion, option is removed from the
		       BootOrder as well.

		  Print details about efivars. By default, all supported efivars are listed,
		  unless valid -f opt is provided.

	HELP
}

is_efivars || exit 1

efivars=("$EFIVARS/"*) efivars_user=() es=0
bpath_type=disk

declare -A setters
declare -A setters_es

setters_es["bactivity_0"]=1
setters_es["bactivity_1"]=2
setters_es["bnext"]=4
setters_es["border"]=8
setters_es["timeout"]=16
setters_es["bpath"]=32
setters_es["dboot"]=64
setters_es["unsupported"]=128

while getopts :a:A:b:Bdf:gG:hlLn:p:P:t:x: arg; do
	case "$arg" in
		a) setters["bactivity_1"]=$OPTARG ;;
		A) setters["bactivity_0"]=$OPTARG ;;
		b) setters["border"]=$OPTARG ;;
		B) efivars=("$EFIVARS/Boot"@([A-F0-9][A-F0-9][A-F0-9][A-F0-9])-"$EFI_GLOBAL_VARIABLE_GUID") ;;
		d) debug=1 ;;
		f)
			mapfile -t _efivars_user < <(get_efivars_user "$OPTARG")
			efivars_user+=("${_efivars_user[@]}")
			;;
		g) efivars=("$EFIVARS/"*"-$EFI_GLOBAL_VARIABLE_GUID") ;;
		G) efivars=("$EFIVARS/"*-"$OPTARG") ;;
		h) help; exit 0 ;;
		l) list=1 ;;
		L) detect_guids list; exit 0 ;;
		n) setters["bnext"]=$OPTARG ;;
		p) setters["bpath"]=$OPTARG ;;
		P) bpath_type=$OPTARG ;;
		t) setters["timeout"]=$OPTARG ;;
		x) setters["dboot"]=$OPTARG ;;
		:) echo "-$OPTARG is missing an argument" >&2; exit 1 ;;
		*) ;;
	esac
done

if ((${#efivars_user[@]} > 0)); then
	efivars=("${efivars_user[@]}")
fi

if ((${#setters[@]} > 0)); then
	if ! isroot; then
		echo "'${!setters[*]}' options require root privileges, bailing" >&2
		exit 1
	fi
	for setter in "${!setters[@]}"; do
		handle_setters "$setter" || ((es |= setters_es["$setter"]))
	done
	exit $((es & 0xff))
elif ((${#efivars[@]} > 0)); then
	if ((list == 1)); then
		list_efivars "${efivars[@]}"
	else
		parse_efivars "${efivars[@]}"
	fi
fi
