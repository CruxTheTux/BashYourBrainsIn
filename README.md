hexdump(1) is the key. Maybe some day Bash will be able to parse
binary data, make $'\0' to be assignable, etc. Though tr(1) + readline
can make a pretty good hexdumper too. ;)
